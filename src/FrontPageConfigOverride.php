<?php

namespace Drupal\fully_multilingual_frontpages;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Overrides front page config.
 */
class FrontPageConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];
    if (in_array('system.site', $names)) {
      $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
      $node = \Drupal::state()->get("fully_multilingual_frontpages.config_page_{$language}");
      $overrides['system.site'] =
        [
          'page' => [
            'front' => '/node/' . $node,
            '403' => '/node/' . \Drupal::state()->get("fully_multilingual_frontpages.config_page_403"),
            '404' => '/node/' . \Drupal::state()->get("fully_multilingual_frontpages.config_page_404"),
          ],
        ];
    }
    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'FrontPageConfigOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}