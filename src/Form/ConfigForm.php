<?php

namespace Drupal\fully_multilingual_frontpages\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\node\Entity\Node;
use Drupal\webform\Entity\Webform;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Search Form.
 */
class ConfigForm extends FormBase {

  protected $container;

  /**
   * Class constructor.
   */
  public function __construct(ContainerInterface $container) {
    $this->container = $container;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'fully_multilingual_frontpages_config_form';
  }

  public function getLanguageConfig(){
    $result = \Drupal::service('language_manager')->getLanguages();

    foreach($result as $key => $value){
      $result[$key] = ['title' => $this->t('%lang front page node', ['%lang' => strtoupper($key)])];
    }

    $result['403'] = ['title' =>$this->t('403 error page')];
    $result['404'] = ['title' =>$this->t('404 error page')];
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['container_pages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Pages settings'),
    ];

    foreach ($this->getLanguageConfig() as $key => $item) {
      $form['container_pages']['config_page_' . $key] = [
        '#type' => 'entity_autocomplete',
        '#title' => $item['title'],
        '#target_type' => 'node',
        '#default_value' => $this->getDefaultValue('config_page_' . $key),
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $views_clear_cache = FALSE;

    // Save form data.
    $form_values = $form_state->getValues();
    foreach ($form_values as $key => $value) {
      if (!empty($value)) {
        $this->container->get('state')
          ->set("fully_multilingual_frontpages.{$key}", $value);
        $views_clear_cache = TRUE;
      }
    }

    if($views_clear_cache) {
      views_invalidate_cache();
    }
  }

  /**
   * Get the default value.
   *
   * @param $field_name
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\media\Entity\Media|null
   */
  public function getDefaultValue($field_name) {
    $node_id = \Drupal::state()->get("fully_multilingual_frontpages.{$field_name}");
    $default_value = NULL;
    if (!empty($node_id)) {
      $default_value = Node::load($node_id);
    }
    return $default_value;
  }

}
